import getConfig from 'next/config'

type Config = {
  publicRuntimeConfig: {
    [key: string]: string
    APP_URL: string
    SENTRY_DSN: string
    BILLING_URL: string
    BANNER_ANNOUNCEMENT: string
    FEEDBACK_URL: string
    TEST_PHONE_DOMAIN: string
    TEST_PHONE_SERVER: string
    TEST_PHONE_USERNAME: string
    TEST_PHONE_SECRET: string
    TEST_PHONE_DISPLAY_NAME: string
  }
}

export const { publicRuntimeConfig: config } = getConfig() as Config

export const __TEST_PHONE_CONFIG__ = Object.freeze({
  displayName: config.TEST_PHONE_DISPLAY_NAME,
  domain: config.TEST_PHONE_DOMAIN,
  username: config.TEST_PHONE_USERNAME,
  secret: config.TEST_PHONE_SECRET,
  audioElementId: 'remoteAudio',
  server: config.TEST_PHONE_SERVER,
})
