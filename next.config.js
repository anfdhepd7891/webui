/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  publicRuntimeConfig: {
    APP_URL: process.env.APP_URL,
    SENTRY_DSN: process.env.SENTRY_DSN,
    BILLING_URL: process.env.BILLING_URL,
    BANNER_ANNOUNCEMENT: process.env.BANNER_ANNOUNCEMENT,
    FEEDBACK_URL: process.env.FEEDBACK_URL,
    TEST_PHONE_DOMAIN: process.env.TEST_PHONE_DOMAIN,
    TEST_PHONE_SERVER: process.env.TEST_PHONE_SERVER,
    TEST_PHONE_USERNAME: process.env.TEST_PHONE_USERNAME,
    TEST_PHONE_SECRET: process.env.TEST_PHONE_SECRET,
    TEST_PHONE_DISPLAY_NAME: process.env.TEST_PHONE_DISPLAY_NAME,
  },
}
